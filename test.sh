modules:
  fortigate_snmp:
    walk:
      - ifXTable
      -
      - fgSysCpuUsage
      - fgSysMemUsage
      - fgSysDiskUsage
      - fgVpnTunEntStatus
      - fgVpnTunEntPhase1Name
      - fgVpnTunEntPhase2Name
      - fgVpnTunnelUpCount
      - fgHwSensorEntName
      - fgHwSensorEntValue
      -
      - fgVpn
      - fgSystem
      - fgIntf
      - fgInetProto
      - fgHaInfo
    version: 3
    max_repetitions: 25
    timeout: 10s
    auth:
      username: dangnv
      security_level: authNoPriv
      password: dangnv
      auth_protocol: SHA